﻿using System;


namespace RogueGame.Game.Visual
{
    public static class ThreadSafeConsole
    {
        static char[,] buffer;
        static ThreadSafeConsole()
        {
            buffer = new char[Console.WindowWidth, Console.WindowHeight];
            Console.CursorVisible = false;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
        }

        // a szinkronizacio miatt kell
        static object obj = new Object();

        // egy karakter kiirasa
        public static void PrintXY(int x, int y, char character)
        {
            lock (obj)
            {
                if (buffer[x, y] != character)
                {
                    buffer[x, y] = character;
                    Console.SetCursorPosition(x, y);
                    Console.Write(character);
                }
            }
        }

        // egy sor kiirasa
        public static void PrintXY(int x, int y, string text)
        {
            for (int i = 0; i < text.Length; i++)
                PrintXY(x + i, y, text[i]);
        }
    }
}
