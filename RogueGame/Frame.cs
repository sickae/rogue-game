﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Game.Space;
using RogueGame.Game.Rules;
using RogueGame.Game.Visual;
using RogueGame.Game.Automate;

namespace RogueGame.Game.Frame
{
    class Frame
    {
        const int MAP_SIZE_X = 21;
        const int MAP_SIZE_Y = 11;
        const int TREASURES = 10;
        private GameSpace space;
        private bool gameOver;
        private GameClock clock;
        private int foundTreasures;

        public Frame()
        {
            space = new GameSpace(MAP_SIZE_X, MAP_SIZE_Y);
            Generate();
            gameOver = false;
            clock = new GameClock();
        }

        private void Generate()
        {
            Random rnd = new Random();
            for (int i = 0; i < MAP_SIZE_X; i++)
            {
                for (int j = 0; j < MAP_SIZE_Y; j++)
                {
                    if (i == 0 || i == MAP_SIZE_X - 1 || j == 0 || j == MAP_SIZE_Y - 1)
                        new Wall(i, j, space);
                }
            }
            for (int i = 0; i < TREASURES; i++)
            {
                int x = rnd.Next(1, MAP_SIZE_X - 1);
                int y = rnd.Next(1, MAP_SIZE_Y - 1);
                GameElement[] e = space.GetElements(x, y);
                if ((x == 1 && y == 1) && e != null)
                {
                    if (e[0].Size > 0)
                    {
                        i--;
                        continue;
                    }
                }
                Treasure t = new Treasure(x, y, space);
                t.Handler += TreasurePickedUp;
            }
        }

        public void Run()
        {
            ConsoleScreen cs = new ConsoleScreen(space, 0, 0);
            Player p = new Player("Béla", 1, 1, space);
            ConsoleScreen pc = new ConsoleScreen(p, 25, 0);
            AIPlayer ai = new AIPlayer("Kati", 3, 4, space);
            ConsoleScreen aic = new ConsoleScreen(ai, 25, 0);
            EvilAIPlayer eai = new EvilAIPlayer("Laci", 5, 1, space);
            ConsoleScreen eaic = new ConsoleScreen(eai, 25, 0);
            ConsoleScoreboard scb = new ConsoleScoreboard(0, 12, 5);
            clock.Add(ai);
            clock.Add(eai);
            clock.Add(cs);
            clock.Add(pc);
            clock.Add(aic);
            clock.Add(eaic);
            scb.AddPlayer(p);
            p.Handler += PlayerChanged;
            while (!gameOver)
            {
                ConsoleKey key = Console.ReadKey().Key;
                if (key == ConsoleKey.LeftArrow) p.Move(-1, 0);
                if (key == ConsoleKey.RightArrow) p.Move(1, 0);
                if (key == ConsoleKey.UpArrow) p.Move(0, -1);
                if (key == ConsoleKey.DownArrow) p.Move(0, 1);
                if (key == ConsoleKey.Escape) gameOver = true;
                ai.RandomMove();
                eai.RandomMove();
            }
        }

        void TreasurePickedUp(Treasure treasure, Player player) => gameOver = foundTreasures++ == TREASURES ? true : false;

        void PlayerChanged(Player sender, int score, int health) => gameOver = health == 0 ? true : false;
    }
}
