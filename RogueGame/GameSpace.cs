﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Game.Visual;

namespace RogueGame.Game.Space
{
    abstract class GameElement
    {
        private int x;
        private int y;
        protected GameSpace space;
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public abstract double Size { get; }

        public GameElement(int x, int y, GameSpace space)
        {
            this.x = x;
            this.y = y;
            this.space = space;
            space.AddElement(this);
        }

        public abstract void Collide(GameElement e); 
    }

    abstract class FixedGameElement : GameElement
    {
        public FixedGameElement(int x, int y, GameSpace space) : base(x, y, space) { }
    }

    abstract class MovingGameElement : GameElement
    {
        private bool active;
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public MovingGameElement(int x, int y, GameSpace space) : base(x, y, space) { Active = true; }

        public void Replace(int x, int y)
        {
            GameElement[] e = space.GetElements(x, y);
            double cellsize = 0;
            for (int i = 0; i < e.Length; i++)
            {
                e[i].Collide(this);
                Collide(e[i]);
                if (!active) break;
                e = space.GetElements(x, y);
            }
            for (int i = 0; i < e.Length; i++) cellsize += e[i].Size;
            if(cellsize + Size <= 1)
            {
                X = x;
                Y = y;
            }
        }
    }

    class GameSpace : IDisplayable
    {
        private List<GameElement> elements = new List<GameElement>();
        private int sizeX;
        private int sizeY;
        public int SizeX { get; }
        public int SizeY { get; }
        public int[] DisplayableSize { get => new int[] { sizeX, sizeY }; }

        public GameSpace(int sizeX, int sizeY)
        {
            this.sizeX = sizeY;
            this.sizeY = sizeY;
        }

        public void AddElement(GameElement element) => elements.Add(element);

        public void RemoveElement(GameElement element)
        {
            if (elements.Contains(element)) elements.Remove(element);
        }

        public GameElement[] GetElements(int x, int y, int distance)
        {
            List<GameElement> el = new List<GameElement>();
            foreach (GameElement e in elements)
                if (Math.Sqrt(Math.Pow(x - e.X, 2) + Math.Pow(y - e.Y, 2)) <= distance) el.Add(e);
            return el.ToArray();
        }

        public GameElement[] GetElements(int x, int y) => GetElements(x, y, 0);

        public IPrintable[] DisplayableElements()
        {
            List<IPrintable> el = new List<IPrintable>();
            foreach (GameElement e in elements)
                if (e is IPrintable) el.Add(e as IPrintable);
            return el.ToArray();
        }
    }

}
