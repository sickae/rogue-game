﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Game.Frame;

namespace RogueGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Frame f = new Frame();
            f.Run();
        }
    }
}
