﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueGame.Game.Automate
{
    interface IAutomatic
    {
        int RunInterval { get; }

        void Run();
    }
}
